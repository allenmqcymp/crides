## Campus Rides

An app built using Firebase as a backend, Swift as a front-end to provide a ride request system for the university shuttle (jitney)

Technologies used:

- MapKit for maps
- UIKit for layout
- FirebaseAuth for authentication
- Firebase Real-time Database for data storage

