
import UIKit
import Foundation
import MapKit

class MapOverlay : NSObject, MKOverlay {
    var coordinate: CLLocationCoordinate2D
    var boundingMapRect: MKMapRect
    
    init(overlayObj: MapInformation) {
        boundingMapRect = overlayObj.overlayBoundingMapRect
        coordinate = overlayObj.midCoordinate
    }
    
}
