
import Foundation
import UIKit
import Firebase
import MapKit
import CoreLocation
import GoogleSignIn

class JitneyViewController: UIViewController, CLLocationManagerDelegate {
    
    let jitneyRef = Database.database().reference(withPath: "jitney")
    let jitneyStatus = Database.database().reference(withPath: "isRunning")
    let locManager = CLLocationManager()

    
    @IBOutlet weak var jitneyStatusSwitch: UISwitch!
    
    @IBAction func signoutButtonActionDriver(_ sender: Any) {
        do {
            // ensure that the jitney is not running
            jitneyStatus.setValue(false)
            // even though the default state of the switch is off in NIB
            // for good measure, set it too off
            // !TODO - understand why it sets to off even when it was on sign out
            jitneyStatusSwitch.setOn(false, animated: false)
            
            // TODO: add more error handling
            try Auth.auth().signOut()
            
            // this signs out of google provider
            // required because if ommitted, google is still signed in
            GIDSignIn.sharedInstance()?.signOut()
            self.dismiss(animated: true, completion: {
                print("User successfully signed out")
            })
        }
        catch (let error){
            print("Auth sign out failed with \(error)")
        }
    }

    @objc func statusSwitchStateChanged(_ sender: UISwitch!) {
        if sender.isOn {
            jitneyStatus.setValue(true)
        } else {
            jitneyStatus.setValue(false)
        }
    }
    
    func checkLocationAuthorizationStatus() -> Bool {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            return true
        } else {
            locManager.requestWhenInUseAuthorization()
            return false
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let jitneyLocation:CLLocation = locations[0] as CLLocation
        // push the location to firebase
        let jitneyXRef = jitneyRef.child("latitude")
        let jitneyYRef = jitneyRef.child("longitude")
        jitneyXRef.setValue(jitneyLocation.coordinate.latitude)
        jitneyYRef.setValue(jitneyLocation.coordinate.longitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error determining location: \(error)")
    }
    
    
    func determineLocation() {
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        // start getting the location
        if checkLocationAuthorizationStatus() {
            locManager.startUpdatingLocation()
        }
        else {
            // BUG: the first time the app is opened, it doesn't work because checkLocation returns false and enters the else block. After you finish allowing access, start updating location never has a chance to run
            // the second time you open the app, however, if location is already allowed, then it can run
            print("not ready yet")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locManager.delegate = self
        
        //Add Target for UISwitch
        jitneyStatusSwitch.addTarget(self, action: #selector(JitneyViewController.statusSwitchStateChanged(_:)), for: .valueChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.determineLocation()
    }

}
