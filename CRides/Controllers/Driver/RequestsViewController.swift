import Foundation
import UIKit
import Firebase

class RequestsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var editButtonLabel: UIButton!
    
    @IBAction func editButtonAction(_ sender: Any) {
        // toggle isEditing
        isEditingOK = !isEditingOK
        self.tableView.isEditing = isEditingOK
    }
    
    
    let requestRef = Database.database().reference(withPath: "requests")
    // so that we can "dequeue" the requests in order - not dequeuing because firebase doesn't have a notion of order
    let orderedRequestRef = Database.database().reference(withPath: "requests").queryOrdered(byChild: "index")
    
    var requestsArr = [RequestsData]()
    
    var requestObserveHandle : UInt!
    
    var isEditingOK : Bool = false {
        didSet {
            if isEditingOK {
                editButtonLabel.setTitle("Done", for: .normal)
            }
            else {
                editButtonLabel.setTitle("Edit", for: .normal)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // construct a requestData model from a child snapshot
    func constructModelFromChild(_ childSnapshot: DataSnapshot, riderName reqName: String, index idx: Int) -> RequestsData? {
        let requestDict = childSnapshot.value as? [String: Any]
        if let requestDict = requestDict {
            guard let name = requestDict["to"] as? String else {
                print("failed to convert to location to string")
                return nil
            }
            guard let latlong = requestDict["latlong"] as? [String: Any] else {
                print("failed to convert latlong to dictionary")
                return nil
            }
            guard let reqLat = latlong["latitude"] as? Double else {
                print("failed to convert latitude to double")
                return nil
            }
            guard let reqLong = latlong["longitude"] as? Double else {
                print("failed to convert longitude to double")
                return nil
            }
            return RequestsData(riderName: reqName, name: name, latlong: [reqLat, reqLong], index: idx)
        }
        else {
            return nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // allow editing
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.requestsArr[sourceIndexPath.row]
        self.requestsArr.remove(at: sourceIndexPath.row)
        self.requestsArr.insert(movedObject, at: destinationIndexPath.row)
        
        writeBackInOrder()
    }
    
    func writeBackInOrder() {
        // write entire requestsArr back to firebase
        // !TODO - implement a diffing functionality so that writebacks are more efficient
        // ie. only the diffs are changed
        for (i, requestD) in requestsArr.enumerated() {
            // change the index
            let latlong = ["latitude": requestD.latlong[0], "longitude": requestD.latlong[1]]
            requestRef.child(requestD.riderName).setValue(["to": requestD.name, "latlong": latlong, "index": i])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // continually observe the locations of the requests
        requestObserveHandle = orderedRequestRef.observe(.value, with: { snapshot in
            self.requestsArr = []
            
            for (i, child) in snapshot.children.enumerated() {
                let child = child as? DataSnapshot
                if let child = child {
                    let childreq = self.constructModelFromChild(child, riderName: child.key, index: i)
                    if let childreq = childreq {
                        self.requestsArr.append(childreq)
                    }
                    else {
                        print("nil value from childreq")
                        return
                    }
                }
                else {
                    print("failed to extract child snapshot")
                    return
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData();
            }
            
        })
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing {
            return .delete
        }
        return .none
    }
    
    // handles marking requests as complete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let deleteReqRow = indexPath.row
            confirmDelete(deleteReqRow)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?
    {
        return "Complete?"
    }
    
    
    func confirmDelete(_ reqRow: Int) {
        let alert = UIAlertController(title: "Mark request as complete/void?", message: "This will remove the request from the queue", preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: "Mark as Complete", style: .default, handler: { alertAction in
            self.handleDeleteRequest(alertAction: alertAction, requestRow: reqRow)
        })
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            alertAction in return
        })
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func handleDeleteRequest(alertAction: UIAlertAction!, requestRow: Int) {
        // delete the value from the requestsArr, writeback to firebase, then reload the tableview
        print("index to remove is \(requestRow)")
        let requestObj = requestsArr[requestRow]
        requestsArr.remove(at: requestRow)
        // remove value from firebase
        requestRef.child(requestObj.riderName).removeValue(completionBlock: { (err, ref) in
            self.writeBackInOrder()
            self.tableView.reloadData()
        })
        // note that writeback only writes what is IN the requests array at this point

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        requestRef.removeObserver(withHandle: requestObserveHandle)
    }
    
    // data source delegate override methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RequestsTableViewCell", for: indexPath) as? RequestsTableViewCell else {fatalError("The dequeued cell is not an instance of RequestsTableViewCell.")}
        let selFavorite = requestsArr[indexPath.row]
        cell.requestInfoLabel.text = "\(selFavorite.riderName) | TO: \(selFavorite.name)"
        // !TODO - update mins using routing library
        cell.requestTimeLabel.text = "Blah mins"
        return cell
    }
}
