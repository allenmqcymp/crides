
import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn

// if you look at the storyboard, this is the root controller

class Login: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {

    // customize the button by setting properties of the gsigninobject
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBAction func loginButtonAction(_ sender: Any) {
        login()
    }
    
    @IBAction func skipButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func registerButtonAction(_ sender: Any) {
        register()
    }
    
    // NOTE since this is a gidsigninbutton VIEW, we use "value changed"
    // instead of touch up inside
    // touch up inside does NOT work for some reason
    @IBAction func gbuttonsigninAction(_ sender: Any) {
        // BUG: on firebase's end, sometimes the auth state is fired twice
        // which means you have to sign out twice
        GIDSignIn.sharedInstance().signIn()
    }
    
    // delegate protocol methods
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            print("error signing in due to \(error)\n")
            return
        }
        
        // trasnfer authority to firebase - using the google token, get a firebase credential
        // TODO: ?? Is this oAuth?
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authresult, error) in
            if let error = error {
                print("login failed with \(error)")
                return
            }
            if let user = authresult?.user {
                print("Successfully logged in via google")
                self.loginSuccessful(user)
            }
            else {
                return
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        // start the signin process
        
        
        Auth.auth().addStateDidChangeListener() { auth, user in
            if let user = user {
                self.loginSuccessful(user)
            }
            else {
                return
            }
        }
        
    }
    
    // hard code the driver in
    // TODO: fix the hardcoding
    func loginSuccessful(_ user: User) {
        if user.email == "driver@driver.com" {
            self.performSegue(withIdentifier: "jitneydriversegue", sender: self)
            self.emailField.text = nil
            self.passwordField.text = nil
        }
        else {
            self.performSegue(withIdentifier: "ridersegue", sender: self)
            self.emailField.text = nil
            self.passwordField.text = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    func login() {
        if self.emailField.text == "" || self.passwordField.text == "" {
            print("Enter email and/or password")
        }
        else {
            Auth.auth().signIn(withEmail: self.emailField.text!, password: self.passwordField.text!) { (user, error) in

                    if error == nil {
                        print("Successfully logged in via email field")
                    }
                    else {
                        print("Error")
                    }
            }
        }
    }
    
    func register() {
        if self.emailField.text == "" {
            print("Non-empty email required")
        }
        else {
            Auth.auth().createUser(withEmail: self.emailField.text!, password: self.passwordField.text!) { (user, error) in
                
                if error == nil {
                    print("Successfully signed up")
                }
                else {
                    print(error!)
                }
            }
        }
    }
}
