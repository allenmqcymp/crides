import Foundation
import UIKit

// required because I want to set the default tab open as the requests tab
// but am unable to do so through storyboards
// and by default the tabbarcontroller doesn't have a programmatic file
class CustomUITabBarController : UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
    }
}

