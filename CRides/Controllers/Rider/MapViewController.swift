import Foundation
import UIKit
import Firebase
import MapKit
import GoogleSignIn

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var userLocationButton: MKUserTrackingButton!
    
    @IBOutlet weak var jitneyStatusLabel: UILabel!
    
    static var mapRegion : MKCoordinateRegion!
    
    let jitneyRef = Database.database().reference(withPath: "jitney")
    let jitneyStatus = Database.database().reference(withPath: "isRunning")
    
    // implicitly unwrapped optional
    // from https://firebase.googleblog.com/2015/10/best-practices-for-ios-uiviewcontroller_6.html
    // jitneyMarker is not nil when it is used since viewdidload sets it
    var jitneyMarker : JitneyMarker!
    // for unregistering the firebase observers
    var observeLocationHandle : UInt!
    // for unregistering jitney status tracker in firebase
    var observeStatusHandle : UInt!
    
    // for showing the overlay view
    var mapInfo = MapInformation(filename: "MapInformation")

    // create manager to request location authorization
    let locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        return manager
    }()

    
    @IBAction func signoutButtonAction(_ sender: Any) {
        do {
            // TODO: add more error handling
            // https://firebase.google.com/docs/auth/ios/errors
            // this signs out of firebase
            try Auth.auth().signOut()
            
            // this signs out of google provider
            // required because if ommitted, google is still signed in
            GIDSignIn.sharedInstance()?.signOut()
            
            self.dismiss(animated: true, completion: {
                print("the user successfully signed out")
            })
        }
        catch (let error){
            print("Auth sign out failed with \(error)")
        }
    }
    
    
    func setupLocationFromSnapshot(snapshot : DataSnapshot) -> CLLocationCoordinate2D? {
        if let jitneySnap = snapshot.value {
            // force downcast to a dictionary, because we know that jitney snap is a NSDictionary-like object
            if let jitneyData = jitneySnap as? [String: Any] {
                guard
                    let jitneyLat = jitneyData["latitude"] as? CLLocationDegrees else {
                    print("could not convert latitude to CLLocation Degrees")
                    return nil
                }
                guard
                    let jitneyLong = jitneyData["longitude"] as? CLLocationDegrees else {
                    print("could not convert longitude to CLLocation degrees")
                    return nil
                }
                let jitneyLocation = Optional(CLLocationCoordinate2D(latitude: jitneyLat, longitude: jitneyLong))
                return jitneyLocation
            }
            else {
                print("failed to cast json to dictionary")
                return nil
            }
        }
        else {
            print("snapshot was nil")
            return nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // continually observe the locations of the jitney
        observeLocationHandle = jitneyRef.observe(.value, with: { snapshot in
            if let jitneyLocation = self.setupLocationFromSnapshot(snapshot: snapshot) {
                // move the annotation marker to where it belongs
                print("JITNEY LOCATION CHANGED - UPDATING THE MARKER")
                // see the second answer
                // https://stackoverflow.com/questions/14131345/ios-refresh-annotations-on-mapview
                self.mapView.removeAnnotation(self.jitneyMarker)
                self.jitneyMarker.coordinate = jitneyLocation
                self.mapView.addAnnotation(self.jitneyMarker)
                // another async dispatch to the main thread
                // is it better or not??
            }
            else {
                print("jitney location is nil")
            }
        })
        // continually observe the status of the jitney
        observeStatusHandle = jitneyStatus.observe(.value, with: { snapshot in
            if let boolStatus = snapshot.value as? Bool {
                if boolStatus {
                    self.jitneyStatusLabel.text = "Jitney is running"
                }
                else {
                    self.jitneyStatusLabel.text = "Jitney not running"
                }
            }
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        jitneyRef.removeObserver(withHandle: observeLocationHandle)
        jitneyStatus.removeObserver(withHandle: observeStatusHandle)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // define a delegate for the mapView
        mapView.delegate = self
        
        // set the mapView of LocationSearchController (the tableviewcontroller) to the current map instance
        MapViewController.mapRegion = mapView.region
        
        // register the jitney marker visual
        mapView.register(JitneyView.self,
                         forAnnotationViewWithReuseIdentifier:
        MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        mapView.showsUserLocation = true
        
        // set up the show user location button
        let button = MKUserTrackingButton(mapView: mapView)
        button.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        mapView.addSubview(button)
        
        // create a jitney marker
        jitneyMarker = JitneyMarker(coordinate: CLLocationCoordinate2D())
        
        DispatchQueue.main.async {
            self.mapView.addAnnotation(self.jitneyMarker)
        }
        
        mapView.add(MapOverlay(overlayObj: mapInfo))
        
    }
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    

    // this will center the map and show a MKMarker Annotation
    //of the jitney's current position
    @IBAction func getJitneyLocationAction(_ sender: Any) {
        jitneyRef.observeSingleEvent(of: .value, with: { snapshot in
            if let jitneyLocation = self.setupLocationFromSnapshot(snapshot: snapshot) {
                print("centering map on location \(jitneyLocation)")
                self.centerMapOnLocation(location: jitneyLocation)
            }
            else {
                print("failed to fetch CLLocation for jitney")
            }
        })
    }
}

extension MapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MapOverlay {
            return MapOverlayView(overlay: overlay, overlayImage: UIImage(imageLiteralResourceName: "campusOverlay"))
        }
        return MKOverlayRenderer()
    }
}
