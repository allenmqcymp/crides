import Foundation
import UIKit
import Firebase

class FavoritesViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let requestRef = Database.database().reference(withPath: "requests")
    let userFavRef = Database.database().reference(withPath: "userfavs")
    
    @IBOutlet weak var requestRideButton: UIButton!
    @IBOutlet weak var rideRequestStatusLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "FavoritesTableViewCell"
    
    var requestRideStatusHandle : UInt!
    
    // whether this user currently signed in has requested a ride or not
    // we make a firebase query to see if key exists in viewdidload
    var requestedRide : Bool! {
        
        didSet {
            if !requestedRide {
                self.rideRequestStatusLabel.text = "No ride currently requested"
                self.requestRideButton.setTitle("Request Ride", for: UIControlState.normal)
            }
            else {
                // the user has already requested a ride
                // extract name from database
                requestRef.observeSingleEvent(of: .value, with: { snapshot in
                    if let email = self.reqEmail {
                        let requestDict = snapshot.childSnapshot(forPath: email).value as? [String: Any]
                        if let requestDict = requestDict {
                            guard let name = requestDict["to"] as? String else {
                                print("failed to convert to location to string")
                                return
                            }
                            self.rideRequestStatusLabel.text = "You've requested a ride to \(name)"
                            self.requestRideButton.setTitle("Cancel Ride", for: UIControlState.normal)
                        }
                        else {
                            print("failed to get child snapshot")
                            return
                        }
                    }
                })
            }
        }
        
    }
    
    var favoritesArr = [favoritesEntry]()
        
    var reqEmail : String?
    var reqUID : String?
    
    // set it to an invalid number on start
    // when cell is selected, the indexPath.row is set to selectedCellNumber
    var selectedCellNumber : Int = -1
    
    
    func loadFavs() -> Void {
        if let uid = getUID() {
            userFavRef.observeSingleEvent(of: .value, with: { snapshot in
                if !snapshot.hasChild(uid) {
                    _ = Favorites(uid)
                    self.fetchFavs(UID: uid, completion: { favs in
                        self.favoritesArr = favs
                        DispatchQueue.main.async {
                            self.tableView.reloadData();
                        }
                    })
                }
                else {
                    self.fetchFavs(UID: uid, completion: { favs in
                        self.favoritesArr = favs
                        DispatchQueue.main.async {
                            self.tableView.reloadData();
                        }
                    })
                }
            })
        }
        else {
            print("could not determine UID")
        }
    }


    
    
    // helper function to fetch favs from firebase
    func fetchFavs(UID uid:String, completion: @escaping ([favoritesEntry]) -> Void) {
        self.userFavRef.child(uid).observeSingleEvent(of: .value, with: { snapshot in
            var temp_favsArr = [favoritesEntry]()
            for child in snapshot.children {
                let child = child as? DataSnapshot
                if let child = child {
                    let childSnapshot = child.value as? [String: [Double]]
                    if let childVals = childSnapshot {
                        let latlong = childVals["latlong"]
                        if let latlong = latlong {
                            assert(latlong.count == 2)
                            temp_favsArr.append(favoritesEntry(name: child.key, latlong: latlong))
                        }
                        else {
                            print("failed to extract double array for latlong")
                        }
                    }
                    else {
                        print("failed to extract child snapshot")
                    }
                }
            }
            completion(temp_favsArr)
        })
    }
    
    
    @IBAction func requestButtonRideAction(_ sender: Any) {
        if !self.requestedRide {
            let selectedRow = tableView.indexPathForSelectedRow?.row
            if let selectedRow = selectedRow {
                let requestEntry : favoritesEntry = favoritesArr[selectedRow]
                handleConfirmRequest(requestEntry) { name in
                    // closure handles change of labels on text
                    // and updates @ @IBOutlet weak var rideRequestStatusLabel
                    self.requestedRide = true
                }
            }
            else {
                print("couldn't retrieve cell")
                return
            }
        }
        else {
            // cancel the ride under their name
            handleCancelRequest() {
                self.requestedRide = false
            }
        }
    }
    
    func getEmail() -> String? {
        let user = Auth.auth().currentUser
        if let user = user {
            for info in user.providerData {
                if info.email != nil {
                    let email = info.email!
                    var reqEmail = ""
                    for (_, char) in email.enumerated() {
                        if char == "@" {
                            break
                        }
                        else {
                            reqEmail.append(char)
                        }
                    }
                    return reqEmail
                }
                else {
                    print("BUG: the email associated with \(user.uid) is nil")
                    return nil
                }
            }
        }
        else {
            print("no one is signed in")
            return nil
        }
        return nil
    }
    
    // get the unique identifier associated with a user
    func getUID() -> String? {
        let user = Auth.auth().currentUser
        if let user = user {
            return user.uid
        }
        else {
            print("no one is signed in")
            return nil
        }
    }
    
    func handleCancelRequest(_ handler: @escaping () -> Void) {
        guard let reqEmail = self.getEmail() else {
            print("could not retrieve email in handleCancelRequest")
            return
        }
        self.requestRef.child(reqEmail).removeValue(completionBlock: { (err, ref) in
            if let err = err {
                print("the jitney driver may have marked ride as complete already (thus deleting value)")
                print(err)
            }
            handler()
        })
        // make sure we set UI stuff only after database is cleared
    }
    
    func handleConfirmRequest(_ request: favoritesEntry, _ handler: @escaping (String) -> Void) {
        let alert = UIAlertController(title: "Confirm RIDE to \(request.name)", message: "Confirm?", preferredStyle: .actionSheet)
        
        let ConfirmAction = UIAlertAction(title: "Confirm", style: .default, handler: { alertAction in
            guard let reqEmail = self.getEmail() else {
                print("could not retrieve email in handleConfirmRequest")
                return
            }
            let latlong = ["latitude": request.latlong[0], "longitude": request.latlong[1]]
            // !CAUTION
            // we index from reverse order so that -1 is the last request!!
            // !TODO - pretty hacky logic - although snapshot should return
            // since email is static, the child value will be overwritten
            // allowing only one request per person to be made at a time
            // hacky set it to some large number
            self.requestRef.observeSingleEvent(of: .value, with: { snapshot in
                self.requestRef.child(reqEmail).setValue(["to": request.name, "latlong": latlong, "index": snapshot.childrenCount])
                handler(request.name)
            })

        })
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { alertAction in
            return
        })
        
        alert.addAction(ConfirmAction)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // get email of user
        reqEmail = getEmail()
        reqUID = getUID()
        
        // load user's favorite places from database
        // mutates global favoritesArr
        loadFavs()
        
        
        // check if ride requested
        requestRideStatusHandle = requestRef.observe(.value, with: { snapshot in
            if let email = self.reqEmail {
                self.requestedRide = snapshot.hasChild(email) ? true : false
            }
            else {
                print("could not retrieve email - thus couldn't retrieve ride status so false is default")
                self.requestedRide = false
            }
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        requestRef.removeObserver(withHandle: requestRideStatusHandle)
    }
    
    
    // data source delegate override methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritesArr.count
    }

    // solution to reusable cells staying checked
    //https://stackoverflow.com/questions/10192908/uitableview-checkmark-only-one-row-at-a-time
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedCellNumber = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FavoritesTableViewCell else {fatalError("The dequeued cell is not an instance of FavoritesTableViewCell.")}
            
            let selFavorite = favoritesArr[indexPath.row]
            cell.favoritesLabel.text = selFavorite.name
            if indexPath.row == selectedCellNumber {
                cell.accessoryType = .checkmark
            }
            else {
                cell.accessoryType = .none
            }
            return cell
    }
    
}

