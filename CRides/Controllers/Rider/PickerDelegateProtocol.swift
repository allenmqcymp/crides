
// picker class must conform to this protocol to pass data

// delegate protocol to pass data back to riderRequestViewController
protocol PickerViewControllerDelegate : AnyObject {
    func passLocationData(_ locationData : Location)
}
