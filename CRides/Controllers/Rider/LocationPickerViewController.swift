import UIKit
import MapKit
import CoreLocation
import Foundation




class LocationPickerViewController : UIViewController, UIGestureRecognizerDelegate, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    weak var delegate : PickerViewControllerDelegate?
    
    @IBAction func finishedPickingLocationAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        // pass data back to the location data back to the view controller
        if let locationData = locationData {
            delegate?.passLocationData(locationData)
        }
    }
    
    var locationData : Location?
    
    var panRecognzier = UIPanGestureRecognizer()
    
    var centerMapCoord : CLLocationCoordinate2D!
    
    var readyForUpdate : Bool = false
    
    let centerPin = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        mapView.delegate = self
        mapView.isUserInteractionEnabled = true
        
        panRecognzier = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        panRecognzier.delegate = self
        mapView.addGestureRecognizer(panRecognzier)
        
        centerPin.title = "pin1"
        centerMapCoord = mapView.centerCoordinate
        centerPin.coordinate = centerMapCoord
        mapView.addAnnotation(centerPin)
        
        // enable the update
        readyForUpdate = true
        updateLocationPicker()
    }

    
    // gesture recognizer
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func didDragMap(_ sender: UIPanGestureRecognizer) {
        if sender.state == .ended {
            readyForUpdate = true
        }
    }
    
    // take the current location and update the label
    func decodeLocation() {
        if let locationData = self.locationData {
            if let name = locationData.name {
                locationLabel.text = "\(name)"
                
                if let placemark = locationData.placemark {
                    detailLabel.text = parseAddress(selectedItem: placemark)
                }
                else {
                    detailLabel.text = ""
                }
                
                
            }
            else if let placemarkName = locationData.placemark?.name {
                locationLabel.text = "\(placemarkName)"
                // can force unwrap because placemark must not have been nil
                // if the program entered this else if clause
                detailLabel.text = parseAddress(selectedItem: locationData.placemark!)
            }
            else {
                // this allows us to round to 2dp
                let lat = Double(round( (100 * locationData.locationData.coordinate.latitude) / 100 ) )
                let long = Double(round( (100 * locationData.locationData.coordinate.longitude) / 100 ) )
                locationLabel.text = "\(lat),\(long)"
                detailLabel.text = ""
            }
        }
        else {
            locationLabel.text = "drag map to choose location"
        }
    }
    
    func updateLocationPicker() {
        if readyForUpdate {
            readyForUpdate = false
            let centerMapLocation: CLLocation =  CLLocation(latitude: centerMapCoord.latitude, longitude: centerMapCoord.longitude)
            geocoder.reverseGeocodeLocation(centerMapLocation, completionHandler: { response, error in
                // https://github.com/almassapargali/LocationPicker/blob/master/LocationPicker/LocationPickerViewController.swift
                if let error = error as NSError?, error.code != 10 { // ignore cancelGeocode errors
                    // show error and remove annotation
                    let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in }))
                    self.present(alert, animated: true) {
                    }
                    // !TODO catch the error where the frequency of requests is too high
                } else if let placemark = response?.first {
                    // get POI name from placemark if any
                    let name = placemark.areasOfInterest?.first
                    self.locationData = Optional(Location(locationData: centerMapLocation, name: name, placemark: placemark))
                    self.decodeLocation()
                }
            })
        }
    }

   
}


extension LocationPickerViewController : MKMapViewDelegate {
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else {return nil}
        
        let pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
        pin.pinTintColor = MKPinAnnotationView.redPinColor()
        pin.canShowCallout = true
        
        return pin
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        // get the new center coordinate of the map
        centerMapCoord = mapView.centerCoordinate
        // display the mkpin at that coordinate
        centerPin.coordinate = centerMapCoord
    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        updateLocationPicker()
    }
}


