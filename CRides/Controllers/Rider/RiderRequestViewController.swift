import Foundation
import UIKit
import Firebase
import MapKit
import GoogleSignIn
import Foundation
import CoreLocation



// Allen Ma 2019

class RiderRequestViewController: UIViewController {
    
    var resultSearchController:UISearchController? = nil
    
    var locationSearchTable: LocationSearchController? = nil
    
    // !TODO - current location takes too much time to get...
    // need some time of waiting, or update UI immediately
    var currentLocation : CLLocation? = nil
    
    @IBOutlet weak var requestRideButton: UIButton!
    
    let requestRef = Database.database().reference(withPath: "requests")
    
    // not sure if I can just re-use the one from MapViewController
    let locationManager = CLLocationManager()
    
    
    var reqUID : String? = nil
    
    @IBAction func getCurrentLocationAction(_ sender: Any) {
        locationManager.requestLocation() // catch the updated location in the delegate methods
    }
    
    var chosenLocation : Location? = nil {
        didSet {
            if let loc = chosenLocation {
                if fromButtonSelected {fromLocation = loc} else {toLocation = loc}
            }
        }
    }
    
    // !TODO - pass location back to the mapviewcontroller - showing cancel or request button
    // - also need to pass location to favorites button
    var requestedRide : Bool = false {
        didSet {
            
        }
    }
    
    
    // returns the "common name" associated with a location
    // return order is:
    // POI name, placemark name, coordinates
    func getCommonName(_ location:Location) -> String {
        if let name = location.name {
            return name
        }
        else if let name = location.placemark?.name {
            return name
        }
        else {
            return "\(location.locationData.coordinate.latitude), \(location.locationData.coordinate.longitude)"
        }
    }
    
    // !TODO - refactor location picking code into utility functions
    var fromLocation : Location? = nil {
        didSet {
            if let locationData = fromLocation {
                if fromButtonSelected {
                    let buttonText = getCommonName(locationData)
                    fromButton.setTitle(buttonText, for: UIControlState.normal)
                }
            }
        }
    }
    
    var toLocation : Location? = nil {
        didSet {
            if let locationData = toLocation {
                if !fromButtonSelected {
                    let buttonText = getCommonName(locationData)
                    toButton.setTitle(buttonText, for: UIControlState.normal)
                }
            }
        }
    }
    
    @IBAction func requestRideAction(_ sender: Any) {
        if !self.requestedRide {
            // check if fromLocation and toLocation are both set
            guard let fromLocation = fromLocation, let toLocation = toLocation else {
                let alert = UIAlertController(title: "Request Incomplete", message: "Need to fill in FROM and TO fields", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in }))
                self.present(alert, animated: true) {
                    return
                }
                return
            }
            
            // make a firebase request
            handleConfirmRequest(from: fromLocation, to: toLocation) {
                self.requestedRide = true
            }
        }
        else {
            handleCancelRequest() {
                self.requestedRide = false
            }
        }
    }
    
    func handleCancelRequest(_ handler: @escaping () -> Void) {
        if let uid = reqUID {
            self.requestRef.child(uid).removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print(err.localizedDescription)
                    print("jitney may have already marked ride as complete")
                }
                handler()
            })
        }
        else {
            print("could not retrieve uid")
            return
        }
    }
    
    func handleConfirmRequest(from fromLocation:Location, to toLocation:Location, _ handler: @escaping () -> Void) {
        let commonNameFrom = getCommonName(fromLocation)
        let commonNameTo = getCommonName(toLocation)
        let alert = UIAlertController(title: "Confirm Request?", message: "From: \(commonNameFrom), To: \(commonNameTo)", preferredStyle: .actionSheet)
        let ConfirmAction = UIAlertAction(title: "Confirm", style: .default, handler: { _ in return })
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in return })
        alert.addAction(ConfirmAction)
        alert.addAction(CancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    @IBOutlet weak var fromButton: UIButton!
    @IBOutlet weak var toButton: UIButton!
    
    // if fromButton is not selected, then it must be toButton
    var fromButtonSelected : Bool = true {
        didSet {
            if fromButtonSelected {
                fromButton.backgroundColor = .green
                toButton.backgroundColor = .gray
            }
            else {
                fromButton.backgroundColor = .gray
                toButton.backgroundColor = .green
            }
        }
    }
    
    // cancel the request during the decision-making process
    // note that cancelling an already requested ride is done in mapviewcontroller
    @IBAction func cancelRequestAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            print("request cancelled")
        })
    }
    
    @IBAction func fromButtonPressed(_ sender: Any) {
        fromButtonSelected = true
    }
    
    
    @IBAction func toButtonPressed(_ sender: Any) {
        fromButtonSelected = false
    }
    
    // helper function to get the user ID
    // get the unique identifier associated with a user
    func getUID() -> String? {
        let user = Auth.auth().currentUser
        if let user = user {
            return user.uid
        }
        else {
            print("error fetching user ID")
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // instantiate locationsearchcontroller programmatically
        locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchController") as! LocationSearchController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        
        // configures the search bar and embeds it in the navigation controller
        let searchBar = resultSearchController!.searchBar
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        
        
        // for passing data from searchcontroller back to this class
        locationSearchTable?.delegate = self
        
//        self.view.addSubview(searchBar)
        // enable autolayout
//        searchBar.translatesAutoresizingMaskIntoConstraints = false

        // add constraints for the searchbar -- do this later, for now, the searchbar is on the top of the nav
//        let searchBarWidth = searchBar.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0)
//        let searchBarHeight = searchBar.heightAnchor.constraint(equalToConstant: 18)
//        let xPlacementSearchbar = searchBar.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
//        var yPlacementSearchbar : NSLayoutConstraint
//        if let titleView = self.navigationItem.titleView {
//            print("titleView available")
//            yPlacementSearchbar = searchBar.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 100)
//        }
//        else {
//            print("titleView not available")
//            yPlacementSearchbar = searchBar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 150)
//        }
//
//        let searchBarCons : [NSLayoutConstraint] = [xPlacementSearchbar, yPlacementSearchbar, searchBarWidth, searchBarHeight]
//        NSLayoutConstraint.activate(searchBarCons)
        
        // condigure appearance of the searchbar
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
                
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        
        reqUID = getUID()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "setLocationOnMapSegue" {
            guard let locationPickerViewController = segue.destination as? LocationPickerViewController else { return }
            locationPickerViewController.delegate = self
        }
    }
    
}

extension RiderRequestViewController : PickerViewControllerDelegate {
    func passLocationData(_ locationData: Location) {
        chosenLocation = locationData
    }
}

extension RiderRequestViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocation = manager.location else { return }
        chosenLocation = Location(locationData: locValue, name: "current location", placemark: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as NSError? { // ignore cancelGeocode errors
            // show error and remove annotation
            let alert = UIAlertController(title: "Failed to get location", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in }))
            self.present(alert, animated: true) {
            }
        }
    }
}
