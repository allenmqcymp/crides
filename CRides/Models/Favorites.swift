// Allen Ma - January 25 2019

import Foundation
import Firebase

// enter some basic places

// walmart 44.568644, -69.644443
// hannaford 44.568815, -69.640969
// pugh 44.563082, -69.661946
// alfond commons 44.551001, -69.630698
// mcdonalds (close to highway) 44.545340, -69.675942
// starbucks 44.570585, -69.642216


class Favorites : NSObject {
    
    let userFavRef = Database.database().reference(withPath: "userfavs")
    
    let defaultFavorites : [String: [Double]] = [
        "Walmart": [44.568644, -69.644443],
        "Hannaford": [44.568815, -69.640969],
        "Pugh Center": [44.563082, -69.661946],
        "Alfond Commons": [44.551001, -69.630698],
        "McDonalds - I95": [44.545340, -69.675942],
        "Starbucks": [44.570585, -69.642216]
    ]
    
    
    init(_ uid:String) {
        super.init()
        
        // push all the favorites to database
        for (name, latlong) in defaultFavorites {
            self.userFavRef.observeSingleEvent(of: .value, with: { snapshot in
                self.userFavRef.child(uid).child(name).setValue(["latlong": latlong])
            })
        }

    }

}


