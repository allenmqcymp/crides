// Allen Ma - December 30 2018

import UIKit
import Foundation
import MapKit

// https://www.raywenderlich.com/425-mapkit-tutorial-overlay-views

class MapInformation {
    var midCoordinate = CLLocationCoordinate2D()
    var overlayTopLeftCoordinate = CLLocationCoordinate2D()
    var overlayTopRightCoordinate = CLLocationCoordinate2D()
    var overlayBottomRightCoordinate = CLLocationCoordinate2D()
    
    // computed property - imagine a small rectangle on the surface of the earth :) - Otto
    var overlayBottomLeftCoordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2DMake(overlayBottomRightCoordinate.latitude,
                                              overlayTopLeftCoordinate.longitude)
        }
    }
    
    // create the bounding rect for the overlay
    var overlayBoundingMapRect: MKMapRect {
        get {
            let topLeft = MKMapPointForCoordinate(overlayTopLeftCoordinate)
            let topRight = MKMapPointForCoordinate(overlayTopRightCoordinate)
            let bottomLeft = MKMapPointForCoordinate(overlayBottomLeftCoordinate)
            
            return MKMapRectMake(
                topLeft.x,
                topLeft.y,
                fabs(topLeft.x - topRight.x),
                fabs(topLeft.y - bottomLeft.y))
        }
    }
    
    init(filename: String) {
        guard let properties = MapInformation.plist(filename) as? [String : Any] else {return}
        
        midCoordinate = MapInformation.parseCoord(dict: properties, fieldName: "midCoord")
        overlayTopLeftCoordinate = MapInformation.parseCoord(dict: properties, fieldName: "overlayTopLeftCoord")
        overlayTopRightCoordinate = MapInformation.parseCoord(dict: properties, fieldName: "overlayTopRightCoord")
        overlayBottomRightCoordinate = MapInformation.parseCoord(dict: properties, fieldName: "overlayBottomRightCoord")
        
    }
    
    static func parseCoord(dict: [String: Any], fieldName: String) -> CLLocationCoordinate2D {
        guard let coord = dict[fieldName] as? String else {
            return CLLocationCoordinate2D()
        }
        let point = CGPointFromString(coord)
        return CLLocationCoordinate2DMake(CLLocationDegrees(point.x), CLLocationDegrees(point.y))
    }
    
    // helper function to deserialize a plist
    class func plist(_ plist: String) -> Any? {
        let filePath = Bundle.main.path(forResource: plist, ofType: "plist")!
        let data = FileManager.default.contents(atPath: filePath)!
        return try! PropertyListSerialization.propertyList(from: data, options: [], format: nil)
    }
    
}
