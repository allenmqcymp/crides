import CoreLocation

// location object that is shared between:
// riderRequestViewController - interprets location data and pushes request onto firebase
// locationpickerviewcontroller - passes data back to riderRequestViewController


struct Location {
    var locationData : CLLocation
    var name : String?
    var placemark : CLPlacemark?
}
