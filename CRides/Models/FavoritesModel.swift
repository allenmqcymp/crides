
import Foundation

// this is used as an entry in the favorites section for the rider
// name is name of location requested
struct favoritesEntry {
    var name : String
    var latlong : [Double]
}

// !TODO - there's probably a better way to structure the data
// this is used as a data container for requests from firebase

// name is name of location requested
struct RequestsData {
    var riderName : String
    var name: String
    var latlong: [Double]
    var index: Int
}
