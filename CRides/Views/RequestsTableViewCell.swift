
import Foundation
import UIKit

class RequestsTableViewCell : UITableViewCell {
    
    @IBOutlet weak var requestInfoLabel: UILabel!
    @IBOutlet weak var requestTimeLabel: UILabel!
}

