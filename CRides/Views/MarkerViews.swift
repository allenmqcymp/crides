// Alen Ma
// 29 Dec 2018

import Foundation
import MapKit

// the visual representation of the jitney
class JitneyView : MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            canShowCallout = false
            image = UIImage(named: "marker")
        }
    }
}

// the mkannotation object
class JitneyMarker : NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        super.init()
    }
}
